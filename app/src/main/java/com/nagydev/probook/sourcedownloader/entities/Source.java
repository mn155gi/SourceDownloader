package com.nagydev.probook.sourcedownloader.entities;

import java.io.Serializable;

/**
 * Created by ProBook on 6. 10. 2016.
 */
public class Source implements Serializable {

    //private String name;
    private String url;
    private String body;
    private String date;

    /**
     * Reprezents the source code
     *
     * @param url
     * @param body the downloaded source
     */
    public Source(String url, String body, String date) {
        //this.name = name;
        this.url = url;
        this.body = body;
        this.date = date;
    }

    /*public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }*/

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
