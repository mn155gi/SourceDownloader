package com.nagydev.probook.sourcedownloader;

import com.nagydev.probook.sourcedownloader.helpers.SourceGetter;

import junit.framework.Assert;

import org.junit.Test;

import java.io.IOException;

/**
 * Created by lenov on 02.11.2017.
 */

public class SourceGetterTest {

    @Test
    public void getSourceTest() {
        try {
            String source = SourceGetter.getSourceByUrl("https://google.com");
            Assert.assertNotNull(source);
            Assert.assertFalse("Src: " + source, source.length() == 0);
        } catch (IOException e) {
            Assert.fail("Test nepresiel: " + e.getMessage());
        }
    }

    /**
     * Manualny test ked je internet vypnuty
     */
    @Test
    public void getSourceTestWhenOffline() {
        try {
            SourceGetter.getSourceByUrl("https://google.com");
        } catch (IOException e) {
            // Ignore -> test ma failnut ked je vypnuty internet
        }
    }

}
