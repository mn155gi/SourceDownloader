package com.nagydev.probook.sourcedownloader.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nagydev.probook.sourcedownloader.R;
import com.nagydev.probook.sourcedownloader.adapters.DbFragmentAdapter;
import com.nagydev.probook.sourcedownloader.database.Database;
import com.nagydev.probook.sourcedownloader.entities.Source;

import java.util.ArrayList;

/**
 * Created by ProBook on 25. 6. 2017.
 */

public class DatabaseFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // TODO bolo by lepsie databazu riesit komplet staticky
        Database database = new Database(getContext());
        ArrayList<Source> sources = database.getList();

        View view = inflater.inflate(R.layout.database_fragment, container, false);
        ListView listview = (ListView) view.findViewById(R.id.database_listview);
        listview.setAdapter(new DbFragmentAdapter(getContext(), sources));

        return view;
    }

}
