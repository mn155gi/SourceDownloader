package com.nagydev.probook.sourcedownloader.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.nagydev.probook.sourcedownloader.entities.Source;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

/**
 * Created by ProBook on 6. 10. 2016.
 */
public class SaveLoad {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public void verifyStoragePermissions(Activity activity) {

        int writePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public int saveFile(String name, String url, String body, String date) throws IOException {
        if (url == null || body == null || name == null) {
            return -11;
        }
        String saveString = "<!--" + url + "-->\n<!--" + date + "-->\n" + body;
        File docsFolder = new File(System.getenv("EXTERNAL_STORAGE"), "/Documents");
        boolean isPresent = true;
        if (!docsFolder.exists()) {
            isPresent = docsFolder.mkdir();
        }
        if (isPresent) {
            File file = new File(System.getenv("EXTERNAL_STORAGE"), "/Documents/" + name);
            file.createNewFile();
            try {
                FileOutputStream fos = new FileOutputStream(file);
                try {
                    PrintStream printStream = new PrintStream(fos);
                    printStream.print(saveString);
                    printStream.close();
                    fos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    return -2;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return -3;
            }
        }
        return -5;
    }

    public int saveSource(ArrayList<Source> source) throws IOException {
        if (source == null) {
            return -4;
        }
        File docsFolder = new File(System.getenv("EXTERNAL_STORAGE"), "/Documents");
        boolean isPresent = true;
        if (!docsFolder.exists()) {
            isPresent = docsFolder.mkdir();
        }
        if (isPresent) {
            File file = new File(System.getenv("EXTERNAL_STORAGE"), "/Documents/sources");
            file.createNewFile();
            try {
                FileOutputStream fos = new FileOutputStream(file);
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(source);
                    oos.close();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return -2;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return -3;
            }
        }
        return -5;
    }

    @SuppressWarnings("unchecked")
    public ArrayList<Source> load() {
        FileInputStream fis;
        try {
            fis = new FileInputStream(System.getenv("EXTERNAL_STORAGE") + "/Documents/sources");
        } catch (FileNotFoundException ex) {
            return null;
        }
        ObjectInputStream ois;
        try {
            ois = new ObjectInputStream(fis);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        try {
            ArrayList<Source> sourceList;
            sourceList = (ArrayList<Source>) ois.readObject();
            ois.close();
            fis.close();
            return sourceList;
        } catch (Exception e) {
            return null;
        }
    }
}
