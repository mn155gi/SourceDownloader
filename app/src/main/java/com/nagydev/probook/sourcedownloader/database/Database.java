package com.nagydev.probook.sourcedownloader.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nagydev.probook.sourcedownloader.entities.Source;

import java.util.ArrayList;

/**
 * Created by ProBook on 25. 6. 2017.
 */

public class Database {

    protected static final String DATABASE_NAME = "srcdwnldrdb"; // source downloader database
    protected static final int DATABASE_VERSION = 1;

    protected static final String TB_NAME = "source_codes";


    public static final String COLUMN_URL = "url";
    public static final String COLUMN_SOURCE = "source";
    public static final String COLUMN_DATE = "dateplustime";


    private SQLiteOpenHelper openHelper;

    static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TB_NAME + " ("
                    + COLUMN_URL + " TEXT PRIMARY KEY,"
                    + COLUMN_SOURCE + " BLOB,"
                    + COLUMN_DATE + " TEXT "
                    + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TB_NAME);
            onCreate(db);
        }
    }

    public Database(Context ctx) {
        openHelper = new DatabaseHelper(ctx);
    }

    public static final String[] columns = {COLUMN_URL, COLUMN_SOURCE, COLUMN_DATE};

    // Vrati celu tabulku
    public Cursor getNotes() {
        SQLiteDatabase db = openHelper.getReadableDatabase();
        return db.query(TB_NAME, columns, null, null, null, null, null);
    }

    public void insertSources(Source... sources) {
        for (Source s : sources) {
            insertSourceObject(s);
        }
    }

    public void insertSourceObject(Source s) {
        insertSource(s.getUrl(), s.getBody(), s.getDate());
    }


    public void insertSource(String url, String source, String date) {
        SQLiteDatabase db = openHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_URL, url);
        values.put(COLUMN_SOURCE, source);
        values.put(COLUMN_DATE, date);


        db.insert(TB_NAME, null, values);
        db.close();
    }

    public void updateSource(String url, String source, String date, String newUrl, String newSource, String newDate) {
        SQLiteDatabase db = openHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_URL, url);
        values.put(COLUMN_SOURCE, source);
        values.put(COLUMN_DATE, date);

        db.update(TB_NAME, values, null, null);
        db.close();
    }

    public void deleteItem(Source source) {
        ArrayList<Source> sources = getList();
        deleteAll();
        if (sources.contains(source)) {
            for (Source s : sources) {
                if (s != source) {
                    insertSource(s.getUrl(), s.getBody(), s.getDate());
                }
            }
        }
    }

    public void deleteAll() {
        SQLiteDatabase db = openHelper.getWritableDatabase();
        db.execSQL("DELETE FROM " + TB_NAME);
        db.close();
    }

    // TODO: neskor spravit aj update aj delete


    public ArrayList<Source> getList() {
        Source src;
        ArrayList<Source> list = new ArrayList<>();
        Cursor note = this.getNotes();
        int urlIndex = note.getColumnIndex(Database.COLUMN_URL);
        int sourceIndex = note.getColumnIndex(Database.COLUMN_SOURCE);
        int dateIndex = note.getColumnIndex(Database.COLUMN_DATE);
        while (note.moveToNext()) {
            src = new Source(note.getString(urlIndex), note.getString(sourceIndex), note.getString(dateIndex));
            list.add(src);
        }
        return list;
    }

}
