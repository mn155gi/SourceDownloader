package com.nagydev.probook.sourcedownloader.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nagydev.probook.sourcedownloader.R;
import com.nagydev.probook.sourcedownloader.activities.FileReaderActivity;
import com.nagydev.probook.sourcedownloader.database.Database;
import com.nagydev.probook.sourcedownloader.entities.Source;
import com.nagydev.probook.sourcedownloader.helpers.SaveLoad;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ProBook on 9. 10. 2016.
 */
public class SourceListerFragment extends Fragment {

    private Database database;
    private View view;
    private ListView listView;
    private ArrayList<Source> downloadedList;
    private ArrayList<String> urlList = new ArrayList<>();
    private ArrayAdapter adapter;
    private SaveLoad saveload;
    private EditText nameInput;
    private TextView nazov;
    private TextView ulozitAko;
    private Button htmlButton;
    private Button txtButton;
    private Button elseButton;
    private Button bckButton;
    private TextView downloadedString;
    private int saveAsPosition = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        database = new Database(getContext());

        view = inflater.inflate(R.layout.activity_source_reader, container, false);
        nameInput = (EditText) view.findViewById(R.id.name_input);
        nazov = (TextView) view.findViewById(R.id.nazov);
        ulozitAko = (TextView) view.findViewById(R.id.ulozit_ako);
        htmlButton = (Button) view.findViewById(R.id.html);
        txtButton = (Button) view.findViewById(R.id.txt);
        elseButton = (Button) view.findViewById(R.id.else_btn);
        bckButton = (Button) view.findViewById(R.id.bck_button);
        downloadedString = (TextView) view.findViewById(R.id.downloaded_string);

        bckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideButtons();
            }
        });

        htmlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fileName;
                if (nameInput.length() > 0) {
                    fileName = nameInput.getText().toString() + ".html";
                    saveSourceFile(fileName);
                    listView.setVisibility(View.VISIBLE);
                    downloadedString.setVisibility(View.VISIBLE);
                    nazov.setVisibility(View.GONE);
                    ulozitAko.setVisibility(View.GONE);
                    nameInput.setVisibility(View.GONE);
                    htmlButton.setVisibility(View.GONE);
                    txtButton.setVisibility(View.GONE);
                    elseButton.setVisibility(View.GONE);
                    bckButton.setVisibility(View.GONE);
                    nameInput.setText("");
                    //Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Prosím zadajte názov súboru", Toast.LENGTH_SHORT).show();
                }
            }
        });

        txtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fileName;
                if (nameInput.length() > 0) {
                    fileName = nameInput.getText().toString() + ".txt";
                    saveSourceFile(fileName);
                    listView.setVisibility(View.VISIBLE);
                    downloadedString.setVisibility(View.VISIBLE);
                    nazov.setVisibility(View.GONE);
                    ulozitAko.setVisibility(View.GONE);
                    nameInput.setVisibility(View.GONE);
                    htmlButton.setVisibility(View.GONE);
                    txtButton.setVisibility(View.GONE);
                    elseButton.setVisibility(View.GONE);
                    bckButton.setVisibility(View.GONE);
                    nameInput.setText("");
                } else {
                    Toast.makeText(getActivity(), "Prosím zadajte názov súboru", Toast.LENGTH_SHORT).show();
                }
            }
        });

        elseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fileName;
                if (nameInput.length() > 0) {
                    fileName = nameInput.getText().toString();
                    saveSourceFile(fileName);
                    listView.setVisibility(View.VISIBLE);
                    downloadedString.setVisibility(View.VISIBLE);
                    nazov.setVisibility(View.GONE);
                    ulozitAko.setVisibility(View.GONE);
                    nameInput.setVisibility(View.GONE);
                    htmlButton.setVisibility(View.GONE);
                    txtButton.setVisibility(View.GONE);
                    bckButton.setVisibility(View.GONE);
                    elseButton.setVisibility(View.GONE);
                    nameInput.setText("");
                } else {
                    Toast.makeText(getActivity(), "Prosím zadajte názov súboru", Toast.LENGTH_SHORT).show();
                }
            }
        });

        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    //if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    listView.setVisibility(View.VISIBLE);
                    downloadedString.setVisibility(View.VISIBLE);
                    nazov.setVisibility(View.GONE);
                    ulozitAko.setVisibility(View.GONE);
                    nameInput.setVisibility(View.GONE);
                    htmlButton.setVisibility(View.GONE);
                    txtButton.setVisibility(View.GONE);
                    bckButton.setVisibility(View.GONE);
                    elseButton.setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });
        reload();
        return view;
    }

//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
//    }

    public void reload() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        listView = (ListView) view.findViewById(R.id.reader_list);
        registerForContextMenu(listView);
        saveload = new SaveLoad();
        saveload.verifyStoragePermissions(getActivity());
        downloadedList = saveload.load();
        if (downloadedList == null) {
            downloadedList = new ArrayList<>();
        }
        urlList = new ArrayList<>();
        for (int i = 0; i < downloadedList.size(); i++) {
            urlList.add(downloadedList.get(i).getUrl());
        }
        if (!urlList.isEmpty()) {
            adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, urlList);
            listView.setAdapter(adapter);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent fileReader = new Intent(getActivity(), FileReaderActivity.class);
                String name = downloadedList.get(position).getUrl();
                String source = downloadedList.get(position).getBody();
                Bundle b = new Bundle();
                b.putString("name", name);
                b.putString("source", source);
                fileReader.putExtras(b);
                startActivity(fileReader);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.reader_list) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_list, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.add:
                Intent fileReader = new Intent(getActivity(), FileReaderActivity.class);
                String name = downloadedList.get(info.position).getUrl();
                String source = downloadedList.get(info.position).getBody();
                Bundle b = new Bundle();
                b.putString("name", name);
                b.putString("source", source);
                fileReader.putExtras(b);
                startActivity(fileReader);
                return true;
            case R.id.edit:
                listView.setVisibility(View.GONE);
                downloadedString.setVisibility(View.GONE);
                nazov.setVisibility(View.VISIBLE);
                ulozitAko.setVisibility(View.VISIBLE);
                nameInput.setVisibility(View.VISIBLE);
                htmlButton.setVisibility(View.VISIBLE);
                txtButton.setVisibility(View.VISIBLE);
                elseButton.setVisibility(View.VISIBLE);
                bckButton.setVisibility(View.VISIBLE);
                saveAsPosition = info.position;
                String nameString;
                if (urlList.get(info.position).length() > 20) {
                    nameString = ("Zadajte názov pre:\n" + urlList.get(info.position).substring(0, 20) + "...");
                } else {
                    nameString = ("Zadajte názov pre:\n" + urlList.get(info.position).substring(0, urlList.get(info.position).length()));
                }
                nazov.setText(nameString);
                return true;
            case R.id.delete_downloaded:
                urlList.remove(info.position);
                downloadedList.remove(info.position);
                if (saveload == null) {
                    saveload = new SaveLoad();
                }
                try {
                    saveload.saveSource(downloadedList);
                } catch (IOException e) {
                    handleExceptions(e);
                }
                adapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /*
        @Override
        public void onClick(View v) {
            String fileName;
            switch (v.getId()){
                case KeyEvent.KEYCODE_BACK:
                    hideButtons();
                    break;
                case R.id.html:
                    if(nameInput.length() > 0){
                        fileName = nameInput.getText().toString() + ".html";
                        saveSourceFile(fileName);
                        hideButtons();
                    } else {
                        Toast.makeText(getActivity(), "Prosím zadajte názov súboru", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.txt:
                    if(nameInput.length() > 0){
                        fileName = nameInput.getText().toString() + ".txt";
                        saveSourceFile(fileName);
                        hideButtons();
                    } else {
                        Toast.makeText(getActivity(), "Prosím zadajte názov súboru", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.else_btn:
                    if(nameInput.length() > 0){
                        fileName = nameInput.getText().toString();
                        saveSourceFile(fileName);
                        hideButtons();
                    } else {
                        Toast.makeText(getActivity(), "Prosím zadajte názov súboru", Toast.LENGTH_SHORT).show();
                    }
                    break;
                default: break;
            }
        }
    */
    public void hideButtons() {
        listView.setVisibility(View.VISIBLE);
        downloadedString.setVisibility(View.VISIBLE);
        nazov.setVisibility(View.GONE);
        ulozitAko.setVisibility(View.GONE);
        nameInput.setVisibility(View.GONE);
        htmlButton.setVisibility(View.GONE);
        txtButton.setVisibility(View.GONE);
        elseButton.setVisibility(View.GONE);
        bckButton.setVisibility(View.GONE);
        nameInput.setText("");
    }


    public void saveSourceFile(String name) {
        String source = "";
        String url = "";
        if (saveAsPosition != -1) {
            source = downloadedList.get(saveAsPosition).getBody();
            url = downloadedList.get(saveAsPosition).getUrl();
            Toast.makeText(getActivity(), "Súbor bol uložený do priečinku 'Dokumenty'", Toast.LENGTH_SHORT).show();
            //Toast.makeText(getActivity(), url, Toast.LENGTH_SHORT).show();
        }
        if (saveload == null) {
            saveload = new SaveLoad();
        }
        try {
            saveload.saveFile(name, url, source, new Date().toString());
            database.insertSource(url, source, new Date().toString());
        } catch (IOException e) {
            handleExceptions(e);
        }

        saveAsPosition = -1;
    }

    private void handleExceptions(Exception e) {
        Toast.makeText(getActivity(), "Ups, niečo sa pokazilo", Toast.LENGTH_SHORT).show();
        Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
        e.printStackTrace();
    }

}