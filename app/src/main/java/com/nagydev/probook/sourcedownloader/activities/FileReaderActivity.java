package com.nagydev.probook.sourcedownloader.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.nagydev.probook.sourcedownloader.R;
import com.pddstudio.highlightjs.HighlightJsView;
import com.pddstudio.highlightjs.models.Language;
import com.pddstudio.highlightjs.models.Theme;

public class FileReaderActivity extends AppCompatActivity {

    private TextView nameText;
    private HighlightJsView highlightJsView;
    private String name;
    private String source;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_reader);

        findTextViews();
        Bundle b = getIntent().getExtras();
        highlightJsView.setTheme(Theme.GITHUB);
        highlightJsView.setHighlightLanguage(Language.HTML);
        System.out.println(source);
        name = b.getString("name");
        source = b.getString("source");
        nameText.setText(name);
        highlightJsView.setSource(source);
    }

    private void findTextViews() {
        nameText = (TextView) findViewById(R.id.name_text);
        highlightJsView = (HighlightJsView) findViewById(R.id.highlight_view);
    }
}
