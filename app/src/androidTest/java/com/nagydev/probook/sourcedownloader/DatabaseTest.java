package com.nagydev.probook.sourcedownloader;

import android.support.test.InstrumentationRegistry;

import com.nagydev.probook.sourcedownloader.database.Database;
import com.nagydev.probook.sourcedownloader.entities.Source;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by lenov on 02.11.2017.
 */

public class DatabaseTest {

    private Database db;

    @Before
    public void initDatabase() {
        if (db == null) {
            db = new Database(InstrumentationRegistry.getTargetContext());
        }
        Assert.assertNotNull(db);
    }

    @After
    public void clearData() {
        clear();
    }

    private void clear() {
        if (db.getList().size() != 0) {
            db.deleteAll();
            Assert.assertEquals(0, db.getList().size());
        }
    }

    @Test
    public void insertTest() {
        clear();
        db.insertSource("test", "src", "date");
        Assert.assertEquals(1, db.getList().size());
        Assert.assertEquals("test", db.getList().get(0).getUrl());
        Assert.assertEquals("src", db.getList().get(0).getBody());
        Assert.assertEquals("date", db.getList().get(0).getDate());
    }

    @Test
    public void updateTest() {
        clear();
        db.insertSource("test", "src", "date");
        Assert.assertEquals(1, db.getList().size());
        Assert.assertEquals("test", db.getList().get(0).getUrl());
        db.updateSource("test", "src", "date", "newUrl", "src", "date");
        Assert.assertEquals(1, db.getList().size());
        Assert.assertEquals("newUrl", db.getList().get(0).getUrl());
    }

    @Test
    public void clearAllTest() {
        db.insertSource("test0", "src", "date");
        db.insertSource("test1", "src", "date");
        db.insertSource("test2", "src", "date");
        Assert.assertTrue(db.getList().size() > 2);

        db.deleteAll();
        Assert.assertEquals(0, db.getList().size());
    }

    @Test
    public void selectAllTest() {
        clear();
        db.insertSource("test0", "src", "date");
        Assert.assertEquals(1, db.getList().size());
        db.insertSource("asdf", "asdf", "aasdf");
        Assert.assertEquals(2, db.getList().size());
        db.insertSource("asdfx", "asdf", "asdf");
        Assert.assertEquals(3, db.getList().size());
    }

    @Test
    public void ultimateTest() {
        clear();
        db.insertSource("test0", "src", "date");
        Assert.assertEquals(1, db.getList().size());
        Assert.assertEquals("test0", db.getList().get(0).getUrl());
        Assert.assertEquals("src", db.getList().get(0).getBody());
        Assert.assertEquals("date", db.getList().get(0).getDate());
        db.updateSource("test0", "src", "date", "test0", "testestestest", "date");
        Assert.assertEquals(1, db.getList().size());
        Assert.assertEquals("test0", db.getList().get(0).getUrl());
        Assert.assertEquals("testestestest", db.getList().get(0).getBody());
        Assert.assertEquals("date", db.getList().get(0).getDate());

        db.insertSource("test0", "src", "date");
        db.insertSource("test1", "src", "date");
        db.insertSource("test2", "src", "date");
        Assert.assertEquals(4, db.getList().size());
        Assert.assertEquals("test2", db.getList().get(3).getUrl());
    }

    @Test
    public void insertSourceObjectTest() {
        clear();
        db.insertSourceObject(new Source("testUrl", "testBody", "testDate"));
        Assert.assertEquals("testUrl", db.getList().get(0).getUrl());
        Assert.assertEquals("testBody", db.getList().get(0).getBody());
        Assert.assertEquals("testDate", db.getList().get(0).getDate());
    }

    @Test
    public void insertObjectAndDataTest() {
        clear();
        db.insertSourceObject(new Source("testUrl", "testBody", "testDate"));
        Assert.assertEquals("testUrl", db.getList().get(0).getUrl());
        Assert.assertEquals("testBody", db.getList().get(0).getBody());
        Assert.assertEquals("testDate", db.getList().get(0).getDate());
        clear();
        db.insertSource("testUrl", "testBody", "testDate");
        Assert.assertEquals("testUrl", db.getList().get(0).getUrl());
        Assert.assertEquals("testBody", db.getList().get(0).getBody());
        Assert.assertEquals("testDate", db.getList().get(0).getDate());
        clear();
        db.insertSourceObject(new Source("testUrl", "testBody", "testDate"));
        db.insertSource("testUrl", "testBody", "testDate");
        Assert.assertEquals(2, db.getList().size());
        Assert.assertEquals("testUrl", db.getList().get(0).getUrl());
        Assert.assertEquals("testBody", db.getList().get(0).getBody());
        Assert.assertEquals("testDate", db.getList().get(0).getDate());
        Assert.assertEquals("testUrl", db.getList().get(1).getUrl());
        Assert.assertEquals("testBody", db.getList().get(1).getBody());
        Assert.assertEquals("testDate", db.getList().get(1).getDate());
    }

    @Test
    public void insertMultipleObjectsTest() {
        clear();
        Source src0 = new Source("1", "1", "1");
        Source src1 = new Source("2", "2", "2");
        Source src2 = new Source("3", "3", "3");
        Source src3 = new Source("4", "4", "4");
        db.insertSources(src0, src1, src2, src3);
        Assert.assertEquals(4, db.getList().size());
        Assert.assertEquals("4", db.getList().get(3).getUrl());
    }

    @Test
    public void insertUpdateAndDeleteTest() {
        clear();
        db.insertSources(new Source("url", "body", "date"));
        Assert.assertEquals("url", db.getList().get(0).getUrl());
        Assert.assertEquals("body", db.getList().get(0).getBody());
        Assert.assertEquals("date", db.getList().get(0).getDate());
        db.updateSource("url", "body", "date", "new", "new", "new");
        Assert.assertEquals("new", db.getList().get(0).getUrl());
        Assert.assertEquals("new", db.getList().get(0).getBody());
        Assert.assertEquals("new", db.getList().get(0).getDate());
        db.deleteItem(db.getList().get(0));
        Assert.assertEquals(0, db.getList().size());
    }

    @Test
    public void deleteTest() {
        clear();
        Source src0 = new Source("1", "1", "1");
        Source src1 = new Source("2", "2", "2");
        Source src2 = new Source("3", "3", "3");
        Source src3 = new Source("4", "4", "4");
        db.insertSources(src0, src1, src2, src3);
        Assert.assertEquals(4, db.getList().size());
        db.deleteItem(src2);
        Assert.assertEquals(3, db.getList().size());
        for (Source s : db.getList()) {
            if (s.getUrl().equals("3") && s.getBody().equals("3") && s.getDate().equals("3")) {
                Assert.fail("Objekt sa nevymazal z databazy");
            }
        }
        db.deleteItem(src0);
        for (Source s : db.getList()) {
            if (s.getUrl().equals("1") && s.getBody().equals("1") && s.getDate().equals("1")) {
                Assert.fail("Objekt sa nevymazal z databazy");
            }
        }
    }

    @Test
    public void secondUltimateTest() {
        clear();
        Assert.assertNotNull(db);
        Assert.assertEquals(0, db.getList().size());
        Source src0 = new Source("1", "1", "1");
        db.insertSources(src0, new Source("1", "1", "x"));
        Assert.assertEquals(2, db.getList().size());
        Assert.assertEquals("x", db.getList().get(1).getDate());
        db.deleteItem(src0);
        Assert.assertEquals("x", db.getList().get(0).getDate());
        for (int i = 0; i < 200; i++) {
            db.insertSourceObject(new Source(Integer.toString(i), Integer.toString(i), Integer.toString(i)));
        }
        Assert.assertEquals(201, db.getList().size());
        Assert.assertEquals("x", db.getList().get(0).getDate());
        db.deleteAll();
        Assert.assertEquals(0, db.getList().size());
    }

}
