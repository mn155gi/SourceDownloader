package com.nagydev.probook.sourcedownloader;

import com.nagydev.probook.sourcedownloader.entities.Source;
import com.nagydev.probook.sourcedownloader.helpers.SaveLoad;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest {

    private SaveLoad saveLoad;

    @Before
    public void before() {
        if (saveLoad == null) {
            saveLoad = new SaveLoad();
        }
        if (saveLoad.load() != null) {
            try {
                saveLoad.saveSource(null);
            } catch (IOException e) {
                Assert.fail("Chyba pri cisteni suborov: " + e.getMessage());
            }
        }
    }

    @Test
    public void saveFileTest() {
        Assert.assertNotNull(saveLoad);
        try {
            saveLoad.saveFile("test", "https://testurl.test", "this is a test", new Date().toString());
        } catch (IOException e) {
            Assert.fail("Neda sa ulozit testovaci subor: " + e.getMessage());
        }
    }

    @Test
    public void saveSourceTest() {
        Assert.assertNotNull(saveLoad);
        ArrayList<Source> srcList = new ArrayList<>();
        srcList.add(new Source("test0", "testB", "testC"));
        srcList.add(new Source("test1", "testB", "testC"));
        srcList.add(new Source("test2", "testB", "testC"));
        srcList.add(new Source("test3", "testB", "testC"));
        srcList.add(new Source("test4", "testB", "testC"));
        srcList.add(new Source("test5", "testB", "testC"));

        try {
            saveLoad.saveSource(srcList);
        } catch (IOException e) {
            Assert.fail("Neda sa ulozit testovaci subor: " + e.getMessage());
        }
        Assert.assertNotNull(saveLoad.load());
        Assert.assertEquals(6, saveLoad.load().size());
    }

    @Test
    public void loadTest() {
        Assert.assertNotNull(saveLoad);
        ArrayList<Source> srcList = new ArrayList<>();
        srcList.add(new Source("testA", "testB", "testC"));
        try {
            saveLoad.saveSource(srcList);
        } catch (IOException e) {
            Assert.fail("Neda sa ulozit testovaci subor: " + e.getMessage());
        }
        Assert.assertNotNull(saveLoad.load());
        Assert.assertEquals("testA", saveLoad.load().get(0).getUrl());
        Assert.assertEquals("testB", saveLoad.load().get(0).getBody());
        Assert.assertEquals("testC", saveLoad.load().get(0).getDate());
    }

}