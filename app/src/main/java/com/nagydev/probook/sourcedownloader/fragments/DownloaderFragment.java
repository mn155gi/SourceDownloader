package com.nagydev.probook.sourcedownloader.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nagydev.probook.sourcedownloader.R;
import com.nagydev.probook.sourcedownloader.database.Database;
import com.nagydev.probook.sourcedownloader.entities.Source;
import com.nagydev.probook.sourcedownloader.helpers.SaveLoad;
import com.nagydev.probook.sourcedownloader.helpers.SourceGetter;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeoutException;

/**
 * Created by ProBook on 9. 10. 2016.
 */
public class DownloaderFragment extends Fragment implements View.OnClickListener {

    private TextView outStr;
    private Button downloadBtn;
    private Button addUrlBtn;
    private ListView listView;
    private EditText inputText;
    private TextView listInfo;

    private ArrayList<String> urlArray = new ArrayList<>();
    private ArrayAdapter adapter;
    private String input;

    private SaveLoad saveLoad;

    private ArrayList<Source> saveFile;

    private View view;

    private ProgressDialog dialog;

    private Database database;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.downloader_fragment, container, false);
        saveLoad = new SaveLoad();
        saveLoad.verifyStoragePermissions(getActivity());
        if ((saveFile = saveLoad.load()) == null) {
            saveFile = new ArrayList<>();
        }
        listInfo = (TextView) view.findViewById(R.id.list_info);
        outStr = (TextView) view.findViewById(R.id.outstr);
        outStr.setVisibility(View.GONE);
        inputText = (EditText) view.findViewById(R.id.input_text);
        addUrlBtn = (Button) view.findViewById(R.id.add_url);
        downloadBtn = (Button) view.findViewById(R.id.download_btn);
        listView = (ListView) view.findViewById(R.id.list_view);
        listInfo.setVisibility(View.GONE);
        registerForContextMenu(listView);
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, urlArray);
        addUrlBtn.setOnClickListener(this);
        downloadBtn.setOnClickListener(this);
        listView.setAdapter(adapter);
        database = new Database(getContext());
        return view;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_url:
                input = inputText.getText().toString();
                if (input.length() > 0) {
                    inputText.setText("");
                    urlArray.add(input);
                    adapter.notifyDataSetChanged();
                    listInfo.setVisibility(View.GONE);
                } else {
                    Toast.makeText(getActivity(), "Zadajte URL adresu", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.download_btn:
                if (urlArray.isEmpty()) {
                    Toast.makeText(getActivity(), "Prosím pridajte aspoň jednu URL adresu", Toast.LENGTH_SHORT).show();
                } else {
                    if (!isOnline()) {
                        Toast.makeText(getActivity(), "Žiadny prístup na internet.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    dialog = ProgressDialog.show(getContext(), "Sťahovanie dát", "Prosím počkajte...", true);
                    dialog.show();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    new AsyncOperation().execute("");
                }
                break;
        }

    }

    /**
     * Toto asi nemusim opisovat.
     */
    private class AsyncOperation extends AsyncTask<String, Void, String> {

        String source;
        Exception error;
        SaveLoad saveLoad;
        int numberOfDownloaded = 0;

        @Override
        protected String doInBackground(String... params) {
            String url;

            for (int i = 0; i < 5; i++) {
                try {
                    for (int j = 0; j < urlArray.size(); j++) {
                        url = validateUrl(urlArray.get(j));
                        source = SourceGetter.getSourceByUrl(url);
                        Date date = new Date();
                        Source sourceToSave = new Source(url, source, date.toString());
                        database.insertSource(url, source, date.toString());
                        if (saveFile == null) {
                            saveFile = new ArrayList<>();
                        }
                        saveFile.add(sourceToSave);
                        numberOfDownloaded++;
                    }
                    if (saveLoad == null) {
                        saveLoad = new SaveLoad();
                    }
                    saveLoad.saveSource(saveFile);
                    return source;
                } catch (IOException e) {
                    error = e;
//                    error = e.toString();
                }
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            outStr.setVisibility(View.VISIBLE);
            if (error != null) {
                Toast.makeText(getActivity(), "Ups, niečo sa pokazilo", Toast.LENGTH_SHORT).show();
                if (error instanceof UnknownHostException) {
                    Toast.makeText(getActivity(), "Neznámy hostiteľ", Toast.LENGTH_SHORT).show();
                } else if (error instanceof TimeoutException) {
                    Toast.makeText(getActivity(), "Čas odozvy vypršal", Toast.LENGTH_SHORT).show();
                } else {
                    // toto je iba informacia pre mna aby som mohol rychlejsie testovat apk
                    outStr.setText(error.toString());
                }
            } else {
                outStr.setText("Úspešne stiahnutých: " + numberOfDownloaded + " z/zo " + urlArray.size());
                urlArray.clear();
                adapter.notifyDataSetChanged();
                listInfo.setVisibility(View.VISIBLE);
            }
            this.cancel(true);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.list_view) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_list_delete_only, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.delete:
                urlArray.remove(info.position);
                adapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    private String validateUrl(String url) {
        if (url.contains("http://") || url.contains("https://")) {
            return url;
        } else {
            return "https://" + url;
        }
    }
}