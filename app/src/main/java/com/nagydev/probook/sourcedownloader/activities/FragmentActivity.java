package com.nagydev.probook.sourcedownloader.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.nagydev.probook.sourcedownloader.R;
import com.nagydev.probook.sourcedownloader.adapters.PagerAdapter;
import com.nagydev.probook.sourcedownloader.fragments.DatabaseFragment;
import com.nagydev.probook.sourcedownloader.fragments.DownloaderFragment;
import com.nagydev.probook.sourcedownloader.fragments.SourceListerFragment;

public class FragmentActivity extends AppCompatActivity {

    private SourceListerFragment sourceListerFragment;
    private DatabaseFragment databaseFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        initPaging();
    }

    private void initPaging() {

        final DownloaderFragment downloaderFragment = new DownloaderFragment();
        sourceListerFragment = new SourceListerFragment();
        databaseFragment = new DatabaseFragment();

        downloaderFragment.setRetainInstance(true);
        sourceListerFragment.setRetainInstance(true);
        databaseFragment.setRetainInstance(true);

        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(downloaderFragment);
        pagerAdapter.addFragment(sourceListerFragment);
        pagerAdapter.addFragment(databaseFragment);

        //prinutenie aplikacie orientacie portrait
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setClipToPadding(false);
        viewPager.setPageMargin(12);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        break;

                    case 1:
//                            sourceListerFragment.reload();
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

}
