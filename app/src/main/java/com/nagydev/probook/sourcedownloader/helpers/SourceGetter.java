package com.nagydev.probook.sourcedownloader.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ProBook on 4. 10. 2016.
 */
public class SourceGetter {

//    public static String getSourceByUrl(String url) throws IOException {
//        URL yahoo = new URL(url);
//        URLConnection yc = yahoo.openConnection();
//        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream(), "UTF-8"));
//        String inputLine;
//        StringBuilder a = new StringBuilder();
//        while ((inputLine = in.readLine()) != null) {
//            a.append(inputLine);
//            a.append("\n");
//        }
//        in.close();
//
//        return a.toString();
//    }

    // TODO otestovat funkcnost novej metody
    public static String getSourceByUrl(String urlInString) throws IOException {
        URL url = new URL(urlInString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(15000); // ms
        connection.setReadTimeout(10000); // ms
        connection.connect();
        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            return null;
        }

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line).append('\n');
        }

        return stringBuilder.toString();
    }
}

