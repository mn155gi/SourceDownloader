package com.nagydev.probook.sourcedownloader.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nagydev.probook.sourcedownloader.R;
import com.nagydev.probook.sourcedownloader.entities.Source;

import java.util.ArrayList;

/**
 * Created by ProBook on 25. 6. 2017.
 */

public class DbFragmentAdapter extends BaseAdapter {

    Context context;
    ArrayList<Source> sources;
    private static LayoutInflater inflater = null;

    // TODO dokoncit a otestovat funkcnost
    public DbFragmentAdapter(Context context, ArrayList<Source> sources) {
        this.context = context;
        this.sources = sources;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return sources.size();
    }

    @Override
    public Object getItem(int position) {
        return sources.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.cstm_row, null);
        }
        TextView urlTextView = (TextView) view.findViewById(R.id.url);
        TextView dateTextView = (TextView) view.findViewById(R.id.date);
        urlTextView.setText(sources.get(position).getUrl());
        dateTextView.setText(sources.get(position).getDate());
        return view;
    }
}

